import os
import numpy as np
import csv
import unidecode
import json
import sys
import re

ParseOFF = open("./data/parseOFF.csv", 'r')
ParseFrigo = open("./data/ParseFrigo.csv", 'r')
model = open('models.txt', 'a')

DataOFF = csv.reader(ParseOFF)
DataFrigo = csv.reader(ParseFrigo)

FrigoProduct = []
FrigoProduct_ = []
FrigoId = []
FrigoNumber = 0
listFrigo = {}
FrigoList = list(DataFrigo)

for FRIGO in FrigoList:
    FrigoProduct.append(FRIGO[1])
    FrigoProduct_.append(FRIGO[1])
    FrigoId.append(FRIGO[0])
    FrigoNumber += 1
    FrigoProduct_[FrigoNumber-1] = FrigoProduct_[FrigoNumber-1].lower()
    FrigoProduct_[FrigoNumber-1] = unidecode.unidecode(FrigoProduct_[FrigoNumber-1])
    FrigoProduct_[FrigoNumber-1] = FrigoProduct_[FrigoNumber-1].replace(" ", "_")
    FrigoProduct[FrigoNumber-1] = FrigoProduct[FrigoNumber-1].lower()
    FrigoProduct[FrigoNumber-1] = unidecode.unidecode(FrigoProduct[FrigoNumber-1])
    label = "__label__" + FrigoId[FrigoNumber-1] + "_" + FrigoProduct_[FrigoNumber-1]
    listFrigo.update({FrigoProduct[FrigoNumber-1]:label})

if os.path.getsize("models.txt") > 0:
    os.remove("models.txt")

OFF = list(DataOFF)
rowId = []
dataBarcode = []
dataProduct = []
dataCategories = []
dataOffRow = []
SplitCategories = []
dataCategorie = []
dataCategorieRow = []
for current in range(len(OFF)):
    dataOffRow.append(OFF[current])
    dataBarcode.append(OFF[current][0])
    dataProduct.append(OFF[current][1])
    dataCategories.append(OFF[current][2])
    rowId.append(current+1)
    SplitCategories.append(OFF[current][2].split(","))
for lenCategories in range(len(SplitCategories)):
    for numberInCategories in range(len(SplitCategories[lenCategories])):
        data = str(lenCategories) + str(" ") + str(SplitCategories[lenCategories][numberInCategories])
        dataCategorie.append(SplitCategories[lenCategories][numberInCategories])
        dataCategorieRow.append(lenCategories)
for key in range(len(dataCategorie)):
    dataCategorie[key] = dataCategorie[key].lower()
    dataCategorie[key] = unidecode.unidecode(dataCategorie[key])
    values = listFrigo.get(dataCategorie[key])
    if values:
        # print (values + " " + str(dataProduct[dataCategorieRow[key]]))
        model.write(values + " " + str(dataProduct[dataCategorieRow[key]]) + "\n")
for x in listFrigo:
    frigomodel = str(listFrigo[x]) + " " + x + "\n"
    model.write(frigomodel)