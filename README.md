![texte alt](https://www.frigomagic.com/wp-content/uploads/2020/03/cropped-logo-Frigo-Magic-300x128.png "FrigoMagic")

# Formatting Data and using Fasttext

install python3, pip3. [python3](https://www.python.org/downloads/mac-osx/) | [pip3](https://pip.pypa.io/en/stable/installing/)<br> 
pip3 install unidecode.<br>
pip3 install fasttext.<br>
Download fastText, pretrained-vector. [FastText](https://fasttext.cc/docs/en/support.html) | [Pretrained vector](https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.fr.300.vec.gz) <br>
Get CSV OpenFoodFact. [CSV OpenFoodFact](https://static.openfoodfacts.org/data/fr.openfoodfacts.org.products.csv)<br>

---

ParseAllCSV.py : python3 ParseAllCSV.py -h for usage<br> 
- Useful for parsing files (OpenFoodFact and FrigoMagic)<br>
- Change filepath openFoodFact csv line 11 & line 52<br>
- Usage :  <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python3 ParseAllCSV.py -a / --allcsv : Parse all CSV openFoodFact and create csv with useful data for fastText. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python3 ParseAllCSV.py -f / --frigo : Parse all CSV FrigoMagic. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python3 ParseAllCSV.py -c / --checkcsv : Parse all CSV openFoodFact and checks whether certain elements are present. <br>

---

CreateModel.py : python3 CreateModel.py<br>
- Create model for fasttext (34000 elements). <br>

---

ParseFastText.py : python3 ParseFastText.py -h for usage. <br>
- Useful for train model or predict similarity between OpenFoodFact and FrigoMagic. <br>
- Change filepath pretrained vector line 69, filepath save model line 70 & 80
- Usage :  <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python3 ParseAllCSV.py -f / --findsimilarity : predict similarity and put the data find into a csv for elasticsearch server. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python3 ParseAllCSV.py -t / --trainmodel : fonction who train model and saving. <br>

---

findProduct.py : python3 findProduct.py {barcode}. <br>
- Useful for find product and similarity on csv. <br>

---

Jérémy Quemener
