import os
import csv
import unidecode
import fasttext
import sys
from datetime import datetime

resultFastText = open("./SimilarityFind.csv", 'r')

ResultList = list(resultFastText)

barcode = sys.argv[1]
# code = []
# productName = []
# prediction = []

with open('.//SimilarityFind.csv', encoding="utf8") as file:
    reader = csv.DictReader(file, delimiter = ';')
    for elements in reader:
        code = (elements['Barcode'])
        productName = (elements["Product_name"])
        prediction = (elements["Prediction"])
        img = (elements["images"])
        satfat = (elements["saturated_fat_100g"])
        carbo = (elements["carbohydrates_100g"])
        if code == barcode and prediction != None:
            print (code + " " + productName + " " + prediction+ " "+ img + " saturated_fat_100g " + satfat + " carbohydrate_100g " + carbo)