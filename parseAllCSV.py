import csv
from datetime import datetime
import getopt, sys
import os

# - Parse all csv OpenFoodFact and put useful data for fastText
def parseOFF():
    f = open("./Output/ParseOFF.csv", "a")
    header = "\"barcode\",\"product_name\",\"categories\",\"images\",\"energy_kcal_100g\",\"fat_100g\",\"saturated_fat_100g\",\"carbohydrates_100g\",\"sugars_100g\",\"proteins_100g\",\"salt_100g\"\n"
    f.write(header)
    with open('/Volumes/DOC/OFF.csv', encoding="utf8") as file:
        reader = csv.DictReader(file, delimiter = '\t')
        for row in reader:
            barcode = row['code']
            product_name = row['product_name']
            countries = row['countries_fr']
            categories = row['categories_fr']
            images = row['image_url']
            energy_kcal_100g = row['energy-kcal_100g']
            fat_100g = row['fat_100g']
            saturated_fat_100g = row['saturated-fat_100g']
            carbohydrates_100g =  row['carbohydrates_100g']
            sugars_100g = row['sugars_100g']
            proteins_100g = row['proteins_100g']
            salt_100g = row['salt_100g']
            # if (product_name != "" and product_name != "undefined" and countries != "" and countries == "France"
            # and categories != "" and categories != "undefined" and barcode != "" and barcode != "undefined"):
            data = barcode + "," + "\"" + product_name + "\"" + "," + "\"" + categories + "\"" + "," + "\"" + images + "\"" + "," + "\"" + energy_kcal_100g + "\"" + "," + "\"" + fat_100g + "\"" + "," + "\"" + saturated_fat_100g + "\"" + "," + "\"" + carbohydrates_100g + "\"" + "," + "\"" + sugars_100g + "\"" + "," + "\"" + proteins_100g + "\"" + "," + "\"" + salt_100g + "\"" + "\n"
            f.write(data)
    f.close()

# - Parse csv FrigoMagic - #
def parseFrigo():
    f = open("./Output/ParseFrigo.csv", "a")
    header = "\"barcode\",\"product_name\",\"categories\"\n"
    f.write(header)
    with open('./Output/ingredient.csv', encoding="utf8") as file:
        reader = csv.DictReader(file, delimiter = ',')
        for row in reader:
            code = row['id']
            product_name = row['libelle']
            categories = row['famille']
            data = code + "," + "\"" + product_name + "\"" + "," + "\"" + categories + "\"" + "\n"
            f.write(data)
    f.close()

# - checks whether certain elements are present - #
def parseOFF1():
    f = open("./Output/ParseOFF1.csv", "a")
    header = "\"barcode\",\"product_name\",\"categories\",\"images\",\"energy_kcal_100g\",\"fat_100g\",\"saturated_fat_100g\",\"carbohydrates_100g\",\"sugars_100g\",\"proteins_100g\",\"salt_100g\"\n"
    f.write(header)
    with open('/Volumes/DOC/OFF.csv', encoding="utf8") as file:
        reader = csv.DictReader(file, delimiter = '\t')
        for row in reader:
            barcode = row['code']
            product_name = row['product_name']
            countries = row['countries_fr']
            categories = row['categories_fr']
            images = row['image_url']
            energy_kcal_100g = row['energy-kcal_100g']
            fat_100g = row['fat_100g']
            saturated_fat_100g = row['saturated-fat_100g']
            carbohydrates_100g =  row['carbohydrates_100g']
            sugars_100g = row['sugars_100g']
            proteins_100g = row['proteins_100g']
            salt_100g = row['salt_100g']
            if (product_name != "" and product_name != "undefined" and countries != "" and countries == "France"
            and categories != "" and categories != "undefined" and barcode != "" and barcode != "undefined"):
                data = barcode + "," + "\"" + product_name + "\"" + "," + "\"" + categories + "\"" + "," + "\"" + images + "\"" + "," + "\"" + energy_kcal_100g + "\"" + "," + "\"" + fat_100g + "\"" + "," + "\"" + saturated_fat_100g + "\"" + "," + "\"" + carbohydrates_100g + "\"" + "," + "\"" + sugars_100g + "\"" + "," + "\"" + proteins_100g + "\"" + "," + "\"" + salt_100g + "\"" + "\n"
                f.write(data)
    f.close()

def usage():
    print ("-a / --allcsv : Parse all CSV openFoodFact and create csv with useful data for fastText")
    print ("-f / --frigo : Parse all CSV FrigoMagic")
    print ("-c / --checkcsv : Parse all CSV openFoodFact and checks whether certain elements are present")

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hafc", ["help", "allcsv", "frigo", "checkcsv"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-a", "--allcsv"):
            print("Parse all CSV openFoodFact and create csv with useful data for fastText")
            parseOFF()
        elif o in ("-f", "--frigo"):
            print("Parse all CSV FrigoMagic")
            parseFrigo()
        elif o in ("-c", "--checkcsv"):
            print("Parse all CSV openFoodFact and checks whether certain elements are present")
            parseOFF1()
        else:
            assert False, "unhandled option"

if __name__ == '__main__':
    print ("program start " + str(datetime.now()))
    main()
    print ("program finish " + str(datetime.now()))
