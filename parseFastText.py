import os
import csv
import unidecode
import fasttext
import re
from datetime import datetime
import getopt, sys

# -- initialize data and open files -- #
ParseOFF = open("./Output/ParseOFF.csv", 'r')
ParseFrigo = open("./Output/ParseFrigo.csv", 'r')
DataOFF = csv.reader(ParseOFF)
DataFrigo = csv.reader(ParseFrigo)


# - all pertinent data for fasttext and stock result on csv - # 
FrigoList = list(DataFrigo)
FrigoProduct = []
FrigoId = []
FrigoUndescore = []
CategorieFrigo = []
OffList = list(DataOFF)
OffProNTransf = []
OffProduct = []
OffCode = []
OffCat = []
OffImg = []
OffEnergy = []
OffFat = []
OffSatFat = []
OffCarbohydrates = []
OffSugar = []
OffProteins = []
OffSalt = []

# -- getData for fasttext -- #
def getData():
    print ("get data from db FrigoMagic and OpenFoodFact")
    for Frigo_row in FrigoList:
        Frigo_row[1] = Frigo_row[1].lower()
        Frigo_row[1] = unidecode.unidecode(Frigo_row[1])
        Frigo_row[2] = Frigo_row[2].lower()
        Frigo_row[2] = unidecode.unidecode(Frigo_row[2])
        FrigoProduct.append(Frigo_row[1])
        FrigoUndescore.append(Frigo_row[1])
        FrigoId.append(Frigo_row[0])
        CategorieFrigo.append(Frigo_row[2])
    for Off_row in OffList:
        OffProNTransf.append(Off_row[1])
        Off_row[1] = Off_row[1].lower()
        Off_row[1] = unidecode.unidecode(Off_row[1])
        OffCode.append(Off_row[0])
        OffProduct.append(Off_row[1])
        OffCat.append(Off_row[2])
        OffImg.append(Off_row[3])
        OffEnergy.append(Off_row[4])
        OffFat.append(Off_row[5])
        OffSatFat.append(Off_row[6])
        OffCarbohydrates.append(Off_row[7])
        OffSugar.append(Off_row[7])
        OffProteins.append(Off_row[9])
        OffSalt.append(Off_row[10])
    return FrigoProduct, FrigoUndescore, FrigoId, CategorieFrigo, OffProduct, OffCode, OffCat, OffImg, OffEnergy, OffFat, 
    OffSatFat, OffCarbohydrates, OffSugar, OffProteins, OffSalt, OffProNTransf

# -- train model -- #
def trainmodel():
    print("train model with fastText")
    models = fasttext.train_supervised('models.txt', dim=300, verbose=2, pretrainedVectors="/Volumes/FRIGOMAGIC/cc.fr.300.vec")
    models.save_model("/Volumes/FRIGOMAGIC/FRIGO.bin")

def is_empty(any_structure):
    if any_structure:
        return False
    else:
        return True

# -- Function who predict similarity and put the data find into a csv for elasticsearch server -- #
def FindSimilarity():
    Data = getData()
    print ("Find similarity with FastText")
    # -- find prediction -- #
    models = fasttext.load_model("/Volumes/FRIGOMAGIC/FRIGO.100.bin")
    f = open("./SimilarityFind.csv", "a")
    header = "Barcode\tProduct_name\tingredient_Id\tprobability\timages\tenergy_kcal_100g\tfat\tsaturated_fat_100g\tcarbohydrates_100g\tsugars_100g\tproteins_100g\tsalt_100g\n"
    f.write(header)
    a = 0
    test = ()
    nutriments = []
    for i in OffProduct:
        a += 1
        Row = a
        Barcorde = OffCode[a-1]
        ProductName = OffProNTransf[a-1]
        Categorie = OffCat[a-1]
        Image = OffImg[a-1]
        Fat = OffFat[a-1]
        SatFat = OffSatFat[a-1]
        Carbohydrates = OffCarbohydrates[a-1]
        Sugar = OffSugar[a-1]
        Proteins = OffProteins[a-1]
        Salt = OffSatFat[a-1]
        Energy = OffEnergy[a-1]
        Predict = models.predict(OffProduct[a-1], threshold=0.8)
        FrigoMagicId = []
        Probability = []
        for i in Predict[1]:
            Probability.append(i)
        for x in Predict[0]:
            FrigoMagicId = re.findall('\d+', x)
        writeData = str(Barcorde) + "\t" + str(ProductName) + "\t" + str(FrigoMagicId).strip('[]').strip('\'''\'') + "\t" + str(Probability).strip('[]') + "\t" + str(Image) + "\t" + str(Energy) + "\t" + str(Fat) + "\t" + str(SatFat) + "\t" + str(Carbohydrates) + "\t" + str(Sugar) + "\t" + str(Proteins) + "\t" + str(Salt)+ "\n"
        # - check if probability found between OpenFoodFact and FrigoMagic 

        if is_empty(Predict[1]) == True or ProductName == "undefined" or ProductName == "" or Categorie == "undefined" and Categorie == "":
            # print (Barcorde + " " + ProductName + " " + Categorie)
            writeData = str(Barcorde) + "\t" + str(ProductName) + "\t" + "0" + "\t" + "0" + "\t" + str(Image) + "\t" + str(Energy) + "\t" + str(Fat) + "\t" + str(SatFat) + "\t" + str(Carbohydrates) + "\t" + str(Sugar) +  "\t" + str(Proteins) + "\t" + str(Salt)+ "\n"
            f.write(writeData)
        elif ProductName == "undefined" and ProductName == "" and Categorie == "undefined" and Categorie == "":
            writeData = str(Barcorde) + "\t" + str(ProductName) + "\t" + "0" + "\t" + "0" + "\t" + str(Image) + "\t" + str(Energy) + "\t" + str(Fat) + "\t" + str(SatFat) + "\t" + str(Carbohydrates) + "\t" + str(Sugar) +  "\t" + str(Proteins) + "\t" + str(Salt)+ "\n"
            f.write(writeData)
        else: 
            f.write(writeData)
    f.close()

def usage():
    print("-f / --findsimilarity : predict similarity and put the data find into a csv for elasticsearch server")
    print("-t / --trainmodel : fonction who train model and saving")

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hft", ["help", "findsimilarity", "trainmodel"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-f", "--findsimilarity"):
            print("Parse all CSV openFoodFact and create csv with useful data for fastText")
            FindSimilarity()
        elif o in ("-t", "--trainmodel"):
            print("fonction who train model and saving ")
            trainmodel()
        else:
            assert False, "unhandled option"

if __name__ == '__main__':
    print ("program start " + str(datetime.now()))
    main()
    print ("Similarity found " + str(datetime.now()))
